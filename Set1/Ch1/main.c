/* This script converts hex to base 64.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BINARY_BUFF_SIZE 1024
#define BASE64_BUFF_SIZE 1024

void convert_hex_str_to_binary_str(char *hex_str, char *bin_str);
void convert_binary_str_to_base64_str(char *bin_str, char *base64_str);

char *int_to_binary_str[] = {
  "0000",
  "0001",
  "0010",
  "0011",
  "0100",
  "0101",
  "0110",
  "0111",
  "1000",
  "1001",
  "1010",
  "1011",
  "1100",
  "1101",
  "1110",
  "1111",
};

char *int_to_base64_str[] = {
"A",
"B",
"C",
"D",
"E",
"F",
"G",
"H",
"I",
"J",
"K",
"L",
"M",
"N",
"O",
"P",
"Q",
"R",
"S",
"T",
"U",
"V",
"W",
"X",
"Y",
"Z",
"a",
"b",
"c",
"d",
"e",
"f",
"g",
"h",
"i",
"j",
"k",
"l",
"m",
"n",
"o",
"p",
"q",
"r",
"s",
"t",
"u",
"v",
"w",
"x",
"y",
"z",
"0",
"1",
"2",
"3",
"4",
"5",
"6",
"7",
"8",
"9",
"+",
"/",
};

/* Iterates through hex_str and concatenates a 4-digit binary number
 * in a string to the bin_str.
 *
 * Exits if it finds a character it does not recognize as hex,
 * i.e. not in 0-9a-f.
 * TODO(RG): Does not check to see if bin_str is smaller than the 
 *       resulting string.
 */
void convert_hex_str_to_binary_str(char *hex_str, char *bin_str) {
  int char_value;
  char *as_binary_str;

  for (size_t i = 0; i < strlen(hex_str); ++i) {

    printf("%c\n", hex_str[i]);

    if (hex_str[i] >= '0' && hex_str[i] <= '9') {
      printf("  is digit.\n");
      char_value = hex_str[i] - '0';

    } else if (hex_str[i] >= 'a' && hex_str[i] <= 'f') {
      printf("  is letter a-f.\n");
      char_value = 10 + hex_str[i] - 'a';

    } else {
      printf("  Unrecognized hex character. Exiting.\n");
      exit(1);

    }

    as_binary_str = int_to_binary_str[char_value];
    printf("  %s\n", as_binary_str);
    /* TODO(RG): not sure how to handle if binary_str is not large enough*/
    strcat(bin_str, as_binary_str);
  }
}

void convert_binary_str_to_base64_str(char *bin_str, char *base64_str) {
  int char_value;
  char *as_base64_str;
  size_t bin_str_len = strlen(bin_str);

  if (bin_str_len % 6 != 0) {
    printf("Unrecognized string length. Exiting.");
    exit(1);
  }

  for (size_t i = 0; i < strlen(bin_str) / 6; ++i){
    char_value = 0;
    
    /* dumb implementation */
    if (bin_str[ i * 6 ] == '1') {
      char_value += 32;
    }

    if (bin_str[ i * 6 + 1] == '1') {
      char_value += 16;
    }

    if (bin_str[ i * 6 + 2 ] == '1') {
      char_value += 8;
    }

    if (bin_str[ i * 6 + 3 ] == '1') {
      char_value += 4;
    }

    if (bin_str[ i * 6 + 4 ] == '1'){
      char_value += 2;
    }

    if (bin_str[ i * 6 + 5 ] == '1'){
      char_value += 1;
    }

    printf("  char_value: %d\n", char_value);

    as_base64_str = int_to_base64_str[char_value];
    strcat(base64_str, as_base64_str);
  }
}

int main(){
  char test_input[] = "0123456789abcdef";
  char binary_str[BINARY_BUFF_SIZE] = "";

  convert_hex_str_to_binary_str(test_input, binary_str);

  printf("Full string:\n%s\n", binary_str);

  char test_2[] = "49276d";
  char binary_str_2[BINARY_BUFF_SIZE] = "";
  char base64_str_2[BASE64_BUFF_SIZE] = "";  

  convert_hex_str_to_binary_str(test_2, binary_str_2);
  printf("Test 2 full string\n%s\n", binary_str_2);

  convert_binary_str_to_base64_str(binary_str_2, base64_str_2);
  printf("Test 2a base64 word\n%s\n", base64_str_2);

  char test_3[] = "49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d";
  char binary_str_3[BINARY_BUFF_SIZE] = "";
  char base64_str_3[BASE64_BUFF_SIZE] = "";
  char truth_3[] = "SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t";

  convert_hex_str_to_binary_str(test_3, binary_str_3);
  convert_binary_str_to_base64_str(binary_str_3, base64_str_3);
  printf("Full test base64\n%s\n", base64_str_3);

  printf("Is it correct?\n%s\n", strcmp(base64_str_3, truth_3) == 0 ? "YES!" : "NO!");
  return 0;
}
