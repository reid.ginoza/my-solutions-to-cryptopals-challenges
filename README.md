# My Solutions to Cryptopals Challenges

This is my repo for working through the [Cryptopals Crypto Challenges](https://cryptopals.com/).

## Description
It's a repo for me to learn.
It is not necessarily useful for other people!

## Roadmap
I'm hoping to work through all 8 sets.

## Contributing
Constructive feedback is welcomed.
Merge requests are not.
(It's just a repo for me!)

## License
MIT License.

## Project status
Very casually worked on.